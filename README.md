*Tìm hiểu các file trong phần config:

- app.php:
  + 'name' => env('APP_NAME', 'Laravel') : Tên của ứng dụng
  + 'env' => env('APP_ENV', 'production'): Môi trường ứng dụng
  + 'debug' => (bool) env('APP_DEBUG', false): Truy xuất cấu hình môi trường

- auth.php:
  + 'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ], 
    (Xác thực mặc định)

  + 'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
    ],
    (duy trì trạng thái người dùng trong mỗi lần request bằng cookie, định nghĩa logic để xác thực)
  + 'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\User::class,
        ],
    (lấy ra dữ liệu người dùng từ phía back-end)
  +  'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
            'throttle' => 60,
        ],
    ],
    (Có thể chỉ định nhiều cấu hình đặt lại mật khẩu)

- cache.php:
  + Trong file này, chúng ta có thể xác định driver cho bộ nhớ cache được sử dụng mặc định trong ứng dụng
  + Mặc định, Laravel được cấu hình để sử dụng driver bộ nhớ cache file, tức là sẽ lưu trữ tuần tự các đối tượng dựa
    trên các file theo đường dẫn storage/framework/cache

- database.php:
  + 'default' => env('DB_CONNECTION', 'mysql'): Code ở file này cho biết project đang kết nối tới hệ quản trị CSDL nào.
  + Đồng thời trong file này cũng định nghĩa các xử lý cho việc kết nối tới hệ quản trị CSDL tương ứng. Như là, mySQL, postgreSQL, sqlite, Microsoft SQL Server,..

- filesystems.php:
  + Tại đây, bạn có thể chỉ định đĩa hệ thống tệp mặc định sẽ được sử dụng
  + Có thể cấu hình nhiều đĩa của cùng một trình điều khiển

- queue.php:
  + Queue giúp chúng ta phân phối các task, cân bằng không gây trì trệ cho các task sắp tới.
  + Queue cho phép hoãn lại các task tiêu thụ nhiều thời gian, ví dụ như gửi mail, giúp tăng tốc độ phản hồi cho ứng dụng web.

- session.php:
+ 'driver' => env('SESSION_DRIVER', 'file'): là nơi lưu trữ và truy suất dữ liệu session thông qua các yêu cầu
+ 'lifetime' => env('SESSION_LIFETIME', 120): set timeout cho session là 120'.
